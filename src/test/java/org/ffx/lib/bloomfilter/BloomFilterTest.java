package org.ffx.lib.bloomfilter;

import org.junit.Assert;
import org.junit.Test;

public class BloomFilterTest {

    @Test
    public void testFilterPositive() {
        String toAdd = "abcdefghijklmnopqrstuvwxyz";
        BloomFilter bloomFilter = BloomFilter.build(32);
        for (int i = 0; i < toAdd.length(); i++) {
            bloomFilter.add(toAdd.substring(i, i+1));
        }

        for (int i = 0; i < toAdd.length(); i++) {
            String testStr = toAdd.substring(i, i+1);
            boolean res = bloomFilter.mayContain(testStr);
            Assert.assertTrue("Failed check for: " + testStr, res);
        }
    }

    @Test
    public void testFilterNegative() {
        String toAdd = "bcdfghklmnpqrstvwxz";
        String toCheck = "aeijouy";
        BloomFilter bloomFilter = BloomFilter.build(32);
        for (int i = 0; i < toAdd.length(); i++) {
            bloomFilter.add(toAdd.substring(i, i+1));
        }

        int res = 0;
        for (int i = 0; i < toCheck.length(); i++) {
            String testStr = toCheck.substring(i, i+1);
            if (bloomFilter.mayContain(testStr)) {
                res++;
            }
        }
        Assert.assertTrue("All negative cannot be probable in filter", res < toCheck.length());
    }

    @Test
    public void testClear() {
        String toAdd = "abcdefghijklmnopqrstuvwxyz";
        BloomFilter bloomFilter = BloomFilter.build(32);
        for (int i = 0; i < toAdd.length(); i++) {
            bloomFilter.add(toAdd.substring(i, i+1));
        }

        bloomFilter.clear();

        for (int i = 0; i < toAdd.length(); i++) {
            String testStr = toAdd.substring(i, i+1);
            boolean res = bloomFilter.mayContain(testStr);
            Assert.assertFalse("Failed check for: " + testStr, res);
        }
    }

    @Test
    public void testRemove() {
        String toAdd = "abcdef";
        BloomFilter bloomFilter = BloomFilter.build(32);
        for (int i = 0; i < toAdd.length(); i++) {
            bloomFilter.add(toAdd.substring(i, i+1));
        }
        Assert.assertTrue("'f' has to be in filter", bloomFilter.mayContain("f"));
        bloomFilter.remove("f");
        Assert.assertFalse("'f' has to removed from filter", bloomFilter.mayContain("f"));
    }

    @Test
    public void testMultipleRemoveAndAdd() {
        String toAdd = "abcdef";
        BloomFilter bloomFilter = BloomFilter.build(32);
        for (int i = 0; i < toAdd.length(); i++) {
            bloomFilter.add(toAdd.substring(i, i+1));
        }
        Assert.assertTrue("'f' has to be in filter", bloomFilter.mayContain("f"));
        bloomFilter.remove("f");
        bloomFilter.remove("f");
        bloomFilter.remove("f");
        bloomFilter.remove("f");
        bloomFilter.remove("f");
        Assert.assertFalse("'f' has to removed from filter", bloomFilter.mayContain("f"));
        bloomFilter.add("f");
        Assert.assertTrue("'f' has to be in filter again", bloomFilter.mayContain("f"));
    }

    @Test
    public void testNullSafety() {
        BloomFilter bloomFilter = BloomFilter.build(32);
        bloomFilter.add(null);
        Assert.assertFalse(bloomFilter.mayContain(null));
        bloomFilter.remove(null);
    }
}
