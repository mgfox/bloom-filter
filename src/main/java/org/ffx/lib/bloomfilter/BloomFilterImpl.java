package org.ffx.lib.bloomfilter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

class BloomFilterImpl implements BloomFilter {
    private final int size;
    private final List<AtomicInteger> storage;

    private static class Triple {
        final Integer a;
        final Integer b;
        final Integer c;

        private Triple(Integer a, Integer b, Integer c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }

    public BloomFilterImpl(int size) {
        this.size = size;
        this.storage = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            storage.add(new AtomicInteger(0));
        }
    }

    public void add(Object item) {
        if (item != null) {
            Triple hashes = getHashValues(item);
            storage.get(hashes.a).incrementAndGet();
            storage.get(hashes.b).incrementAndGet();
            storage.get(hashes.c).incrementAndGet();
        }
    }

    public void remove(Object item) {
        if (item != null) {
            Triple hashes = getHashValues(item);
            if (storage.get(hashes.a).decrementAndGet() < 0) {
                storage.get(hashes.a).set(0);
            }
            if (storage.get(hashes.b).decrementAndGet() < 0) {
                storage.get(hashes.b).set(0);
            }
            if (storage.get(hashes.c).decrementAndGet() < 0) {
                storage.get(hashes.c).set(0);
            }
        }
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            storage.get(i).set(0);
        }
    }

    @Override
    public boolean mayContain(Object item) {
        if (item != null) {
            Triple hashes = getHashValues(item);
            return storage.get(hashes.a).get() > 0 &&
                   storage.get(hashes.b).get() > 0 &&
                   storage.get(hashes.c).get() > 0;
        }
        return false;
    }


    private Triple getHashValues(Object item) {
        String s = item.toString();
        return new Triple(hash1(s), hash2(s), hash3(s));
    }

    private int hash1(String item) {
        int hash = 0;
        for (int charIndex = 0; charIndex < item.length(); charIndex++) {
            char c = item.charAt(charIndex);
            hash = (hash << 5) + hash + c;
            hash &= hash; // Convert to 32bit integer
            hash = Math.abs(hash);
        }
        return hash % this.size;
    }

    private int hash2(String item) {
        int hash = 5381;
        for (int charIndex = 0; charIndex < item.length(); charIndex++) {
            char c = item.charAt(charIndex);
            hash = (hash << 5) + hash + c; /* hash * 33 + c */
        }
        return Math.abs(hash % this.size);
    }

    private int hash3(String item) {
        int hash = 0;
        for (int charIndex = 0; charIndex < item.length(); charIndex++) {
            char c = item.charAt(charIndex);
            hash = (hash << 5) - hash;
            hash += c;
            hash &= hash; // Convert to 32bit integer
        }
        return Math.abs(hash % this.size);
    }
}
