package org.ffx.lib.bloomfilter;


public interface BloomFilter {

    /**
     * add item to filter
     * @param item item to add
     */
    void add(Object item);

    /**
     * remove item from filter
     * @param item item to remove
     */
    void remove(Object item);

    /**
     * clear filter
     */
    void clear();

    /**
     * is this item probably in filter list?
     * @param item item to check
     * @return boolean @true if item is probable in filter, @false if item for sure not in filter
     */
    boolean mayContain(Object item);

    static BloomFilter build(int size) {
        return new BloomFilterImpl(size);
    }
}
