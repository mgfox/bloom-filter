# Bloom filter

An implementation of bloom filter with simple extension allowing not only adding items but also to remove them.

inspired by [Bloom filter JavaScript implementation](https://github.com/trekhleb/javascript-algorithms/tree/master/src/data-structures/bloom-filter)

## Usage 

```java

// creating a new bloom filter instance
BloomFilter bloomFilter = BloomFilter.build(1024); // 1024 items size

// adding items to bloom filter 
bloomFilter.add("foo");
bloomFilter.add("bar");

// checking if item in bloom filter
if (bloomFilter.mayContain("foobar")) {
    System.out.println("I think 'foobar' maybe in bloomFilter.")
} else {
    System.out.println("I'm sure 'foobar' is not found in bloomFilter.");
}

// removing items 
bloomFilter.remove("foo");

// giving a bloom filter instance a fresh start 
bloomFilter.clear();
``` 